/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * G_operations.c
 *
 *  Created on: Jan 11, 2019
 *      Author: abanoub
 */

#include "G_operations.h"

bool a = false, b = false, c = false, d = false;

void resetRegisters() {
	a = b = c = d = false;
}

char* G_direct_operation(O_direct_operation *operation) {
	return operation->var;
}

void G_assignment(O_assignment *operation) {
	printf("mov %s, %s\n", G_operation(operation->from), ((O_direct_operation*) operation->to)->var);
	resetRegisters();
}

char* G_addition(O_plus *operation) {
	if (operation->operation1->operation_type == E_direct_operation) {
		if (!a) {
			a = true;
			printf("mov %s, %s\n", ((O_direct_operation*) operation->operation1)->var, G_ACC0);
		} else {
			printf("add %s, %s\n", G_ACC0, ((O_direct_operation*) operation->operation1)->var);
		}
	} else {
		G_operation(operation->operation1);
	}

	if (b) {
		if (!a) {
			printf("mov %s, %s\n", G_ACC1, G_ACC0);
			a = true;
		} else {
			printf("add %s, %s\n", G_ACC0, G_ACC1);
		}
		b = false;
	}

	if (operation->operation2->operation_type == E_direct_operation) {
		printf("add %s, %s\n", G_ACC0, ((O_direct_operation*) operation->operation2)->var);
	} else {
		G_operation(operation->operation2);
	}

	if (b) {
		if (!a) {
			printf("mov %s, %s\n", G_ACC1, G_ACC0);
			a = true;
		} else {
			printf("add %s, %s\n", G_ACC0, G_ACC1);
		}
		b = false;
	}

	return G_ACC0;
}

char* G_subtraction(O_minus *operation) {
	if (operation->operation1->operation_type == E_direct_operation) {
		if (!a) {
			a = true;
			printf("mov %s, %s\n", ((O_direct_operation*) operation->operation1)->var, G_ACC0);
		} else {
			printf("sub %s, %s\n", G_ACC0, ((O_direct_operation*) operation->operation1)->var);
		}
	} else {
		G_operation(operation->operation1);
	}

	if (b) {
		if (!a) {
			printf("mov %s, %s\n", G_ACC1, G_ACC0);
			a = true;
		} else {
			printf("sub %s, %s\n", G_ACC0, G_ACC1);
		}
		b = false;
	}

	if (operation->operation2->operation_type == E_direct_operation) {
		printf("sub %s, %s\n", G_ACC0, ((O_direct_operation*) operation->operation2)->var);
	} else {
		G_operation(operation->operation2);
	}

	if (b) {
		if (!a) {
			printf("mov %s, %s\n", G_ACC1, G_ACC0);
			a = true;
		} else {
			printf("sub %s, %s\n", G_ACC0, G_ACC1);
		}
		b = false;
	}

	return G_ACC0;
}

char* G_multiplication(O_times *operation) {
	if (operation->operation1->operation_type == E_direct_operation) {
		if (!b) {
			b = true;
			printf("mov %s, %s\n", ((O_direct_operation*) operation->operation1)->var, G_ACC1);
		} else {
			printf("mul %s, %s\n", G_ACC1, ((O_direct_operation*) operation->operation1)->var);
		}
	} else {
		G_operation(operation->operation1);
	}

	if (operation->operation2->operation_type == E_direct_operation) {
		printf("mul %s, %s\n", G_ACC1, ((O_direct_operation*) operation->operation2)->var);
	} else {
		G_operation(operation->operation2);
	}

	return G_ACC1;
}

char* G_division(O_divide *operation) {
	if (operation->operation1->operation_type == E_direct_operation) {
		if (!b) {
			b = true;
			printf("mov %s, %s\n", ((O_direct_operation*) operation->operation1)->var, G_ACC1);
		} else {
			printf("div %s, %s\n", G_ACC1, ((O_direct_operation*) operation->operation1)->var);
		}
	} else {
		G_operation(operation->operation1);
	}

	if (operation->operation2->operation_type == E_direct_operation) {
		printf("div %s, %s\n", G_ACC1, ((O_direct_operation*) operation->operation2)->var);
	} else {
		G_operation(operation->operation2);
	}

	return G_ACC1;
}

char* G_incrementation(O_increment *operation) {

}

char* G_decrementation(O_decrement *operation) {

}

char* G_operation(O_operation *operation) {

	if (operation->operation_type == E_direct_operation) {
		return G_direct_operation((O_direct_operation*) operation);
	} else if (operation->operation_type == E_assignment) {
		G_assignment((O_assignment*) operation);
		return "";
	} else if (operation->operation_type == E_addition) {
		return G_addition((O_plus*) operation);
	} else if (operation->operation_type == E_subtraction) {
		return G_subtraction((O_minus*) operation);
	} else if (operation->operation_type == E_multiplication) {
		return G_multiplication((O_times*) operation);
	} else if (operation->operation_type == E_division) {
		return G_division((O_divide*) operation);
	} else if (operation->operation_type == E_incrementation) {
		return G_incrementation((O_increment*) operation);
	} else if (operation->operation_type == E_decrementation) {
		return G_decrementation((O_decrement*) operation);
	} else{
		printf("ignored operation\n");
		return "";
	}

}
