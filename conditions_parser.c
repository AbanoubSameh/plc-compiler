/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * conditions_parser.c
 *
 *  Created on: Dec 17, 2018
 *      Author: abanoub
 */

#include "parser.h"

#include "C_comparison_conditions.h"

#include "conditions_parser.h"

void connectCondition(C_condition *condition, C_condition *toSet) {

	if (condition->condition_type == E_and_condition) {
		setAndSecondCondition((C_and_condition*) condition, toSet);
	} else if (condition->condition_type == E_or_condition) {
		setOrSecondCondition((C_or_condition*) condition, toSet);
	} else if (condition->condition_type == E_not_condition) {
		setNotCondition((C_not_condition*) condition, toSet);
	} else if (condition->condition_type == E_pos_condition) {
		setPosCondition((C_pos_condition*) condition, toSet);
	} else if (condition->condition_type == E_neg_condition) {
		setNegCondition((C_neg_condition*) condition, toSet);
	} else if (toSet->condition_type == E_direct_condition) {

		if (condition->condition_type == E_equal_condition) {
			setEqualSecondCondition((C_equal_condition*) condition, toSet);
		} else if (condition->condition_type == E_nequal_condition) {
			setNequalSecondCondition((C_nequal_condition*) condition, toSet);
		} else if (condition->condition_type == E_gthan_condition) {
			setGthanSecondCondition((C_gthan_condition*) condition, toSet);
		} else if (condition->condition_type == E_sthan_condition) {
			setSthanSecondCondition((C_sthan_condition*) condition, toSet);
		} else if (condition->condition_type == E_ge_condition) {
			setGeSecondCondition((C_ge_condition*) condition, toSet);
		} else if (condition->condition_type == E_se_condition) {
			setSeSecondCondition((C_se_condition*) condition, toSet);
		} else {
			printf("\ntoken: %s\n\n", token);
			syntax_error();
		}

		checkComparisonVariable(condition);

	} else {
		printf("\ntoken here %s\n\n", token);
		syntax_error();
	}

}

C_condition* newCondition(char* typeToken, C_condition *direct) {

	C_condition *toReturn;

	if (isExclamationMark(typeToken)) {
		C_not_condition *condition = newC_not_condition();
		toReturn = (C_condition*) condition;
	} else if (isPos(typeToken)) {
		C_pos_condition *condition = newC_pos_condition();
		toReturn = (C_condition*) condition;
	} else if (isNeg(typeToken)) {
		C_neg_condition *condition = newC_neg_condition();
		toReturn = (C_condition*) condition;
	} else if (isAnd(typeToken)) {
		C_and_condition *condition = newC_and_condition();
		setAndFirstCondition(condition, direct);
		toReturn = (C_condition*) condition;
	} else if (isOr(typeToken)) {
		C_or_condition *condition = newC_or_condition();
		setOrFirstCondition(condition, direct);
		toReturn = (C_condition*) condition;
	} else if (direct->condition_type == E_direct_condition) {

		if (isEqual(typeToken)) {
			C_equal_condition *condition = newC_equal_condition();
			setEqualFirstCondition(condition, direct);
			toReturn = (C_condition*) condition;
		} else if (isNE(typeToken)) {
			C_nequal_condition *condition = newC_nequal_condition();
			setNequalFirstCondition(condition, direct);
			toReturn = (C_condition*) condition;
		} else if (isGT(typeToken)) {
			C_gthan_condition *condition = newC_gthan_condition();
			setGthanFirstCondition(condition, direct);
			toReturn = (C_condition*) condition;
		} else if (isST(typeToken)) {
			C_sthan_condition *condition = newC_sthan_condition();
			setSthanFirstCondition(condition, direct);
			toReturn = (C_condition*) condition;
		} else if (isGE(typeToken)) {
			C_ge_condition *condition = newC_ge_condition();
			setGeFirstCondition(condition, direct);
			toReturn = (C_condition*) condition;
		} else if (isSE(typeToken)) {
			C_se_condition *condition = newC_se_condition();
			setSeFirstCondition(condition, direct);
			toReturn = (C_condition*) condition;
		} else {
			syntax_error();
		}

	} else {
		syntax_error();
	}

	return toReturn;

}

C_condition* newDirectCondition(char *token) {
	C_direct_condition *temp = newC_direct_condition();
	setDirectCondition(temp, token);
	return (C_condition*) temp;
}

C_condition* condition() {

	bool emptyCondition = true;
	bool pass = false;
	C_condition *lastCondition = NULL;
	C_condition *toReturn = NULL;

	while (token != 0) {

		if (!pass) {
			lexer(token);
		} else {
			pass = false;
		}

		if (isCRB(token)) {
			break;
		}
		printf("\ntoken in loop: %s\n\n", token);

		if (isORB(token)) {

			if (lastCondition != NULL) {
				connectCondition(lastCondition, condition(input));
				lastCondition = NULL;
			} else if (toReturn == NULL) {
				toReturn = condition(input);
			} else {
				connectCondition(toReturn, condition(input));
			}

			emptyCondition = false;

		} else if (isExclamationMark(token) || isPos(token) || isNeg(token)) {

			if (lastCondition != NULL) {

				C_condition *temp = newCondition(token, NULL);

				if (lastCondition->condition_type != E_not_condition
						&& temp->condition_type == E_not_condition) {
					connectCondition(lastCondition, temp);
					lastCondition = temp;
				} else {
					syntax_error();
				}

			} else {
				lastCondition = newCondition(token, NULL);

				if (toReturn == NULL) {
					toReturn = lastCondition;
				} else if (toReturn != NULL) {
//					printf("\ntoken: %d\n\n", lastCondition->condition_type);
					connectCondition(toReturn, lastCondition);
				} else {
					syntax_error();
				}

			}

			emptyCondition = true;

		} else if (isAnd(token) || isOr(token)) {

			if (lastCondition != NULL || toReturn == NULL) {
				syntax_error();
			} else {
				toReturn = newCondition(token, toReturn);
			}

			emptyCondition = true;

		} else if (isVariable(token)) {

			C_condition *comparison;
			C_condition *toConnect;
			char tempToken[TOKENLENGTH];
			lexer(tempToken);

			if (isComparisonOperator(tempToken)) {
				comparison = newCondition(tempToken, newDirectCondition(token));
				lexer(tempToken);
				if (isVariable(tempToken)) {
					connectCondition(comparison, newDirectCondition(tempToken));
					toConnect = comparison;
				} else {
					syntax_error();
				}
			} else {
				toConnect = newDirectCondition(token);
				strcpy(token, tempToken);
				pass = true;
			}

			if (lastCondition != NULL) {
//				printf("\ntoken: %s\n\n", token);
				connectCondition(lastCondition, toConnect);
				lastCondition = NULL;
			} else if (toReturn == NULL) {
				toReturn = toConnect;
			} else {
				connectCondition(toReturn, toConnect);
			}

			emptyCondition = false;

		} else if (isSpace(token)) {
			continue;
		} else {
			syntax_error();
		}

	}

	if (lastCondition != NULL || emptyCondition) {
		syntax_error();
	}

	memset(token, 0, TOKENLENGTH);
	return toReturn;

}
