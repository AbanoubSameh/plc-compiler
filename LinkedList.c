/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * linkedList.c
 *
 *  Created on: Dec 1, 2018
 *      Author: abanoub
 */

// right now this file is not used
#include "parser.h"

node *newNode(char* string) {
	node *newNode = malloc(16);
	char *token = malloc(strlen(string) + 1);
	strcpy(token, string);
	newNode->string = token;
	return newNode;
}

void printList(node *root) {
	printf("printing nodes\n");
	node *temp = root;
	while (temp != NULL) {
		printf("%s\n", (*temp).string);
		temp = (*temp).next;
	}
}

void cleanList(node *root) {
	int count = 0;
	node *temp = root;
	while (temp != NULL) {
		node *temp2 = temp->next;
		free(temp->string);
		free(temp);
		temp = temp2;
		count++;
	}
	printf("%d nodes freed\n", count);
}
