################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../C_abstract_syntax_tree.c \
../C_body.c \
../C_comment.c \
../C_comparison_conditions.c \
../C_condition.c \
../C_function_declaration.c \
../C_if_statement.c \
../C_node.c \
../C_string.c \
../G_conditions.c \
../G_operations.c \
../LinkedList.c \
../O_operations.c \
../code_generator.c \
../compiler.c \
../conditions_parser.c \
../copyright.c \
../errors.c \
../escape.c \
../functions.c \
../lexer.c \
../parser.c \
../variables.c 

OBJS += \
./C_abstract_syntax_tree.o \
./C_body.o \
./C_comment.o \
./C_comparison_conditions.o \
./C_condition.o \
./C_function_declaration.o \
./C_if_statement.o \
./C_node.o \
./C_string.o \
./G_conditions.o \
./G_operations.o \
./LinkedList.o \
./O_operations.o \
./code_generator.o \
./compiler.o \
./conditions_parser.o \
./copyright.o \
./errors.o \
./escape.o \
./functions.o \
./lexer.o \
./parser.o \
./variables.o 

C_DEPS += \
./C_abstract_syntax_tree.d \
./C_body.d \
./C_comment.d \
./C_comparison_conditions.d \
./C_condition.d \
./C_function_declaration.d \
./C_if_statement.d \
./C_node.d \
./C_string.d \
./G_conditions.d \
./G_operations.d \
./LinkedList.d \
./O_operations.d \
./code_generator.d \
./compiler.d \
./conditions_parser.d \
./copyright.d \
./errors.d \
./escape.d \
./functions.d \
./lexer.d \
./parser.d \
./variables.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

variables.o: ../variables.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"variables.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


