/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_body.c
 *
 *  Created on: Dec 8, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_body *newC_body() {
	C_body *newC_body = malloc(ENUMSIZE + POINTERSIZE);
	newC_body->type = E_body;
	newC_body->firstNode = NULL;
	printf("new body created at:\t%p\n", newC_body);
	return newC_body;
}

void addBodyNode(C_body *C_body, C_node *node) {
	if (C_body->firstNode == NULL) {
		C_body->firstNode = node;
		printf("body at\t%p\t does not have a first node\n", C_body);
		printf("node at\t%p\tset as first node for body at\t%p\n", node,
				C_body);
	} else {
		C_node *temp = C_body->firstNode;
		while (temp->next != NULL) {
			temp = temp->next;
		}
		temp->next = node;
		printf("body at\t%p\t does has a first node\n", C_body);
		printf("node at\t%p\tset as new node for body at\t%p\n", node, C_body);
	}
}

