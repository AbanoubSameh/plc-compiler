/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_comparison_conditions.h
 *
 *  Created on: Dec 17, 2018
 *      Author: abanoub
 */

// this file was created because the C_main.h is becoming crowded with
// functions and structures and it is becoming harder and harder to
// organize stuff in there, so the comparison conditions will be defined here


#ifndef C_COMPARISON_CONDITIONS_H_
#define C_COMPARISON_CONDITIONS_H_

// this is an equal condition
typedef struct C_equal_condition {
	C_condition_type condition_type;
	char var_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_equal_condition;

// this is a not equal condition
typedef struct C_nequal_condition {
	C_condition_type condition_type;
	char var_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_nequal_condition;

typedef struct C_gthan_condition {
	C_condition_type condition_type;
	char var_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_gthan_condition;

typedef struct C_sthan_condition {
	C_condition_type condition_type;
	char var_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_sthan_condition;

typedef struct C_ge_condition {
	C_condition_type condition_type;
	char var_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_ge_condition;

typedef struct C_se_condition {
	C_condition_type condition_type;
	char var_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_se_condition;

// equal condition

C_equal_condition *newC_equal_condition();
void setEqualFirstCondition(C_equal_condition *C_equal_condition,
		void *condition);
void setEqualSecondCondition(C_equal_condition *C_equal_condition,
		void *condition);

// not equal condition

C_nequal_condition *newC_nequal_condition();
void setNequalFirstCondition(C_nequal_condition *C_nequal_condition,
		void *condition);
void setNequalSecondCondition(C_nequal_condition *C_nequal_condition,
		void *condition);

// greater than condition

C_gthan_condition *newC_gthan_condition();
void setGthanFirstCondition(C_gthan_condition *C_gthan_condition,
		void *condition);
void setGthanSecondCondition(C_gthan_condition *C_gthan_condition,
		void *condition);

// smaller than condition

C_sthan_condition *newC_sthan_condition();
void setSthanFirstCondition(C_sthan_condition *C_sthan_condition,
		void *condition);
void setSthanSecondCondition(C_sthan_condition *C_sthan_condition,
		void *condition);

// greater than or equal condition

C_ge_condition *newC_ge_condition();
void setGeFirstCondition(C_ge_condition *C_ge_condition, void *condition);
void setGeSecondCondition(C_ge_condition *C_ge_condition, void *condition);

// smaller than or equal condition

C_se_condition *newC_se_condition();
void setSeFirstCondition(C_se_condition *C_se_condition, void *condition);
void setSeSecondCondition(C_se_condition *C_se_condition, void *condition);

#endif /* C_COMPARISON_CONDITIONS_H_ */
