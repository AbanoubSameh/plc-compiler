/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * functions.c
 *
 *  Created on: Dec 7, 2018
 *      Author: abanoub
 */

#include "parser.h"
#include "C_comparison_conditions.h"

// function that checks to see if a token begins with a letter or a number
// if it does (if it is a variable), it return true, otherwise it returns false
bool isVariable(char *token) {

	if ((token[0] >= LOWERCASEA && token[0] <= LOWERCASEZ)
			|| (token[0] >= UPPERCASEA && token[0] <= UPPERCASEZ)
			|| (token[0] >= ZERO && token[0] <= NINE)) {
		return true;
	} else {
		return false;
	}

}

// checks if a string is a valid comparison operator: '==', '!=', '>', '<', '>=', '<='
bool isComparisonOperator(char *token) {

	if (!strcmp(token, EQUAL) || !strcmp(token, NE) || !strcmp(token, GE)
	|| !strcmp(token, SE) || token[0] == GREATERTHAN
	|| token[0] == SMALLERTHAN) {
		printf("%s\n", token);
		return true;
	} else {
		return false;
	}

}

// checks if string is '&&'
bool isAnd(char *token) {
	printf("\ntoken and: %s\n\n", token);

	if (!strcmp(token, AND)) {
		return true;
	} else {
		return false;
	}

}

// checks if string is '||'
bool isOr(char *token) {

	if (!strcmp(token, OR)) {
		return true;
	} else {
		return false;
	}

}

// checks if string is '!'
bool isNot(char *token) {

	if (token[0] == EXCLAMATIONMARK && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is 'POS', which is a symbol that corresponds to a positive edge in the PLC
bool isPos(char *token) {

	if (!strcmp(token, POS)) {
		return true;
	} else {
		return false;
	}

}

// checks if string is 'NEG', which is a symbol that corresponds to a negative edge in the PLC
bool isNeg(char *token) {

	if (!strcmp(token, NEG)) {
		return true;
	} else {
		return false;
	}

}

// checks if string is a new line ('\n') character
bool isNewLine(char *token) {

	if (token[0] == NEWLINE && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is a space (' ') or ascii tab
bool isSpace(char *token) {

	if (token[0] == SPACE || token[0] == TAB || token[0] == NEWLINE) {
		return true;
	} else {
		return false;
	}

}

// checks if string is an opening round bracket
bool isORB(char *token) {

	if (token[0] == OROUNDBRACKET && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is a closing round bracket
bool isCRB(char *token) {

	if (token[0] == CROUNDBRACKET && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is an opening square bracket
bool isOSB(char *token) {

	if (token[0] == OSQUAREBRACKET && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is a closing square bracket
bool isCSB(char *token) {

	if (token[0] == CSQUAREBRACKET && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is an opening curly bracket
bool isOCB(char *token) {

	if (token[0] == OCURLYBRACKET && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is a closing curly bracket
bool isCCB(char *token) {

	if (token[0] == CCURLYBRACKET && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

// checks if string is the beginning of a single line comment
bool isComment(char *token) {

	if (!strcmp(token, COMMENT)) {
		return true;
	} else {
		return false;
	}

}

bool isSemicolon(char *token) {

	if (token[0] == SEMICOLON && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

bool isComma(char *token) {

	if (token[0] == COMMA && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

bool isEqualSign(char *token) {

	if (token[0] == EQUALSIGN && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

bool isExclamationMark(char *token) {

	if (token[0] == EXCLAMATIONMARK && token[1] == 0) {
		return true;
	} else {
		return false;
	}

}

bool isFirstConnection(char *connection) {

	if (!strcmp(connection, "LD") || !strcmp(connection, "ld")) {
		return true;
	} else {
		return false;
	}

}

bool isEqual(char *token) {
	if (!strcmp(token, EQUAL)) {
		return true;
	} else {
		return false;
	}
}

bool isNE(char *token) {
	if (!strcmp(token, NE)) {
		return true;
	} else {
		return false;
	}
}

bool isGE(char *token) {
	if (!strcmp(token, GE)) {
		return true;
	} else {
		return false;
	}
}

bool isSE(char *token) {
	if (!strcmp(token, SE)) {
		return true;
	} else {
		return false;
	}
}

bool isGT(char *token) {
	if (token[0] == GREATERTHAN && token[1] == 0) {
		return true;
	} else {
		return false;
	}
}

bool isST(char *token) {
	if (token[0] == SMALLERTHAN && token[1] == 0) {
		return true;
	} else {
		return false;
	}
}

bool isPlusAssign(char *token) {
	if (!strcmp(token, PLUSE)) {
		return true;
	} else {
		return false;
	}
}

bool isMinusAssign(char *token) {
	if (!strcmp(token, MINUSE)) {
		return true;
	} else {
		return false;
	}
}

bool isTimesAssign(char *token) {
	if (!strcmp(token, TIMESE)) {
		return true;
	} else {
		return false;
	}
}

bool isDivideAssign(char *token) {
	if (!strcmp(token, DIVIDEE)) {
		return true;
	} else {
		return false;
	}
}


bool isIncOp(char *token) {
	if (!strcmp(token, INC)) {
		return true;
	} else {
		return false;
	}
}

bool isDecOp(char *token) {
	if (!strcmp(token, DEC)) {
		return true;
	} else {
		return false;
	}
}

bool isSHROp(char *token) {
	if (!strcmp(token, SHR)) {
		return true;
	} else {
		return false;
	}
}


bool isSHLOp(char *token) {
	if (!strcmp(token, SHL)) {
		return true;
	} else {
		return false;
	}
}

bool isAdditionSign(char *token) {
	if (token[0] == PLUSSIGN && token[1] == 0) {
		return true;
	} else {
		return false;
	}
}

bool isSubtractionSign(char *token) {
	if (token[0] == MINUSSIGN && token[1] == 0) {
		return true;
	} else {
		return false;
	}
}

bool isMultiplicationSign(char *token) {
	if (token[0] == ASTERISK && token[1] == 0) {
		return true;
	} else {
		return false;
	}
}

bool isDivisionSign(char *token) {
	if (token[0] == FORWARDSLASH && token[1] == 0) {
		return true;
	} else {
		return false;
	}
}

bool isOperation(char *token) {
	if (isAdditionSign(token) || isSubtractionSign(token)
			|| isMultiplicationSign(token) || isDivisionSign(token)) {
		return true;
	} else {
		return false;
	}
}

char* tabToSpace(char *word) {
	memset(tabTemp, 32, TABSIZE);
	memcpy(tabTemp, word, strlen(word));
	tabTemp[strlen(word)] = 32;
	return tabTemp;
}

char* tabToSpaceSCS(char *word, char letter, char *word2) {
	char let[1];
	let[0] = letter;
	memset(tabTemp, 32, TABSIZE);
	memcpy(tabTemp, word, strlen(word));
	memcpy(tabTemp + strlen(word), let, 1);
	memcpy(tabTemp + strlen(word) + 1, word2, strlen(word2));
	tabTemp[strlen(word) + strlen(word2) + 1] = 32;
	return tabTemp;
}

// this part of the file is so weird and probably needs so much more organization
// and/or to be rewritten. Probably also it needs a lot of optimization

// ** never add any function beyond this point in the file **
bool bracketsCheck(C_condition *condition) {

	C_condition *condition1 = NULL;
	C_condition *condition2 = NULL;

	if (condition->condition_type == E_and_condition) {
		condition1 = ((C_and_condition*) condition)->condition1;
		condition2 = ((C_and_condition*) condition)->condition2;
	} else if (condition->condition_type == E_or_condition) {
		condition1 = ((C_or_condition*) condition)->condition1;
		condition2 = ((C_or_condition*) condition)->condition2;
	} else {
		syntax_error();
	}

	if ((condition1->condition_type == E_and_condition
			|| condition1->condition_type == E_or_condition)
			&& (condition2->condition_type == E_and_condition
					|| condition2->condition_type == E_or_condition)) {
		return true;
	} else {
		return false;
	}

}

bool numIsInteger(char *variable) {

	for (int i = 0; i < strlen(variable); i++) {
		if (!(variable[i] >= ZERO && variable[i] <= NINE)) {
			return false;
		}
	}

	return true;

}

bool isMemoryLocation(char *variable) {

	if ((variable[1] == BYTE[0])
			|| (variable[1] == WORD[0]
					|| ((variable[0] == 'T' || variable[0] == 'C')
							&& numIsInteger(variable + 1)))
			|| (variable[1] == DOUBLE[0])) {
		return true;
	} else {
		return false;
	}

}

bool numIsFloat(char *variable) {

	for (int i = 0; i < strlen(variable); i++) {
		if (!((variable[i] >= ZERO && variable[i] <= NINE) || variable[i] == DOT)) {
			return false;
		}
	}

	return true;

}

char getVariableType(char *variable) {

	if (numIsInteger(variable)) {
		return INTEGER[0];
	} else if (numIsFloat(variable)) {
		return REAL[0];
	} else if (variable[1] == BYTE[0]) {
		return BYTE[0];
	} else if (variable[1] == WORD[0] || ((variable[0] == 'T' || variable[0] == 'C') && numIsInteger(variable+1))) {
		return WORD[0];
	} else if (variable[1] == DOUBLE[0]) {
		return DOUBLE[0];
	} else {
		variable_type_error();
		return 0;
	}

}

char varType(char *var1, char *var2) {
	char c1 = getVariableType(var1);
	char c2 = getVariableType(var2);

	if (c1 == c2) {
		return c1;
	} else if (c1 == INTEGER[0] && c2 != INTEGER[0]) {
		return c2;
	} else if (c2 == INTEGER[0] && c1 != INTEGER[0]) {
		return c1;
	} else if (c1 == INTEGER[0] && c2 == INTEGER[0]) {
		return DOUBLE[0];
	} else if ((c1 == DOUBLE[0] && c2 == REAL[0]) || (c2 == DOUBLE[0] && c1 == REAL[0])) {
		return REAL[0];
	} else {
		variable_type_error();
		return 0;
	}
}

void checkComparisonVariable(C_condition *condition) {

	char *first;
	char *second;

	if (condition->condition_type == E_equal_condition) {
		first =
				((C_direct_condition*) ((C_equal_condition*) condition)->condition1)->var;
		second =
				((C_direct_condition*) ((C_equal_condition*) condition)->condition2)->var;

		 ((C_equal_condition*) condition)->var_type = varType(first, second);
	} else if (condition->condition_type == E_nequal_condition) {
		first =
				((C_direct_condition*) ((C_nequal_condition*) condition)->condition1)->var;
		second =
				((C_direct_condition*) ((C_nequal_condition*) condition)->condition2)->var;

		((C_nequal_condition*) condition)->var_type = varType(first, second);
	} else if (condition->condition_type == E_gthan_condition) {
		first =
				((C_direct_condition*) ((C_gthan_condition*) condition)->condition1)->var;
		second =
				((C_direct_condition*) ((C_gthan_condition*) condition)->condition2)->var;

		((C_gthan_condition*) condition)->var_type = varType(first, second);
	} else if (condition->condition_type == E_sthan_condition) {
		first =
				((C_direct_condition*) ((C_sthan_condition*) condition)->condition1)->var;
		second =
				((C_direct_condition*) ((C_sthan_condition*) condition)->condition2)->var;

		((C_sthan_condition*) condition)->var_type = varType(first, second);
	} else if (condition->condition_type == E_ge_condition) {
		first =
				((C_direct_condition*) ((C_ge_condition*) condition)->condition1)->var;
		second =
				((C_direct_condition*) ((C_ge_condition*) condition)->condition2)->var;

		((C_ge_condition*) condition)->var_type = varType(first, second);
	} else if (condition->condition_type == E_se_condition) {
		first =
				((C_direct_condition*) ((C_se_condition*) condition)->condition1)->var;
		second =
				((C_direct_condition*) ((C_se_condition*) condition)->condition2)->var;

		((C_se_condition*) condition)->var_type = varType(first, second);
	} else {
		variable_type_error();
	}

}
