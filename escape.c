/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * escape.c
 *
 *  Created on: Dec 1, 2018
 *      Author: abanoub
 */

#include "parser.h"

char escape(char esc) {

	if (esc == 97) {
		return 7;
	} else if (esc == 98) {
		return 8;
	} else if (esc == 102) {
		return 12;
	} else if (esc == 110) {
		return 10;
	} else if (esc == 114) {
		return 13;
	} else if (esc == 116) {
		return 9;
	} else if (esc == 118) {
		return 11;
	} else if (esc == 92) {
		return 92;
	} else if (esc == 39) {
		return 39;
	} else if (esc == 34) {
		return 34;
	} else if (esc == 63) {
		return 63;
	} else {
		return 32;
	}

}

