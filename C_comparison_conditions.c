/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_comparison_conditions.c
 *
 *  Created on: Dec 17, 2018
 *      Author: abanoub
 */

#include "C_main.h"
#include "C_comparison_conditions.h"

// equal condition
C_equal_condition *newC_equal_condition() {
	C_equal_condition *newC_equal_condition = malloc(
			ENUMSIZE + 2 * POINTERSIZE + 1);
	newC_equal_condition->condition_type = E_equal_condition;
	newC_equal_condition->condition1 = NULL;
	newC_equal_condition->condition2 = NULL;
	newC_equal_condition->var_type = 0;
	printf("new equal condition created at:\t%p\n", newC_equal_condition);
	return newC_equal_condition;
}

void setEqualFirstCondition(C_equal_condition *C_equal_condition,
		void *condition) {
	C_equal_condition->condition1 = condition;
	printf(
			"condition at\t%p\tset as first condition for equal condition at\t%p\n",
			condition, C_equal_condition);
}

void setEqualSecondCondition(C_equal_condition *C_equal_condition,
		void *condition) {
	C_equal_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for equal condition at\t%p\n",
			condition, C_equal_condition);
}

// not equal condition

C_nequal_condition *newC_nequal_condition() {
	C_nequal_condition *newC_nequal_condition = malloc(
			ENUMSIZE + 2 * POINTERSIZE + 1);
	newC_nequal_condition->condition_type = E_nequal_condition;
	newC_nequal_condition->condition1 = NULL;
	newC_nequal_condition->condition2 = NULL;
	newC_nequal_condition->var_type = 0;
	printf("new nequal condition created at:\t%p\n", newC_nequal_condition);
	return newC_nequal_condition;
}

void setNequalFirstCondition(C_nequal_condition *C_nequal_condition,
		void *condition) {
	C_nequal_condition->condition1 = condition;
	printf(
			"condition at\t%p\tset as first condition for nequal condition at\t%p\n",
			condition, C_nequal_condition);
}

void setNequalSecondCondition(C_nequal_condition *C_nequal_condition,
		void *condition) {
	C_nequal_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for nequal condition at\t%p\n",
			condition, C_nequal_condition);
}

// greater than condition

C_gthan_condition *newC_gthan_condition() {
	C_gthan_condition *newC_gthan_condition = malloc(
			ENUMSIZE + 2 * POINTERSIZE + 1);
	newC_gthan_condition->condition_type = E_gthan_condition;
	newC_gthan_condition->condition1 = NULL;
	newC_gthan_condition->condition2 = NULL;
	newC_gthan_condition->var_type = 0;
	printf("new gthan condition created at:\t%p\n", newC_gthan_condition);
	return newC_gthan_condition;
}

void setGthanFirstCondition(C_gthan_condition *C_gthan_condition,
		void *condition) {
	C_gthan_condition->condition1 = condition;
	printf(
			"condition at\t%p\tset as first condition for gthan condition at\t%p\n",
			condition, C_gthan_condition);
}

void setGthanSecondCondition(C_gthan_condition *C_gthan_condition,
		void *condition) {
	C_gthan_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for gthan condition at\t%p\n",
			condition, C_gthan_condition);
}

// smaller than condition

C_sthan_condition *newC_sthan_condition() {
	C_sthan_condition *newC_sthan_condition = malloc(
			ENUMSIZE + 2 * POINTERSIZE + 1);
	newC_sthan_condition->condition_type = E_sthan_condition;
	newC_sthan_condition->condition1 = NULL;
	newC_sthan_condition->condition2 = NULL;
	newC_sthan_condition->var_type = 0;
	printf("new sthan condition created at:\t%p\n", newC_sthan_condition);
	return newC_sthan_condition;
}

void setSthanFirstCondition(C_sthan_condition *C_sthan_condition,
		void *condition) {
	C_sthan_condition->condition1 = condition;
	printf(
			"condition at\t%p\tset as first condition for sthan condition at\t%p\n",
			condition, C_sthan_condition);
}

void setSthanSecondCondition(C_sthan_condition *C_sthan_condition,
		void *condition) {
	C_sthan_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for sthan condition at\t%p\n",
			condition, C_sthan_condition);
}

// greater than or equal condition

C_ge_condition *newC_ge_condition() {
	C_ge_condition *newC_ge_condition = malloc(ENUMSIZE + 2 * POINTERSIZE + 1);
	newC_ge_condition->condition_type = E_ge_condition;
	newC_ge_condition->condition1 = NULL;
	newC_ge_condition->condition2 = NULL;
	newC_ge_condition->var_type = 0;
	printf("new ge condition created at:\t%p\n", newC_ge_condition);
	return newC_ge_condition;
}

void setGeFirstCondition(C_ge_condition *C_ge_condition, void *condition) {
	C_ge_condition->condition1 = condition;
	printf("condition at\t%p\tset as first condition for ge condition at\t%p\n",
			condition, C_ge_condition);
}

void setGeSecondCondition(C_ge_condition *C_ge_condition, void *condition) {
	C_ge_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for ge condition at\t%p\n",
			condition, C_ge_condition);
}

// smaller than or equal condition

C_se_condition *newC_se_condition() {
	C_se_condition *newC_se_condition = malloc(ENUMSIZE + 2 * POINTERSIZE + 1);
	newC_se_condition->condition_type = E_se_condition;
	newC_se_condition->condition1 = NULL;
	newC_se_condition->condition2 = NULL;
	newC_se_condition->var_type = 0;
	printf("new se condition created at:\t%p\n", newC_se_condition);
	return newC_se_condition;
}

void setSeFirstCondition(C_se_condition *C_se_condition, void *condition) {
	C_se_condition->condition1 = condition;
	printf("condition at\t%p\tset as first condition for se condition at\t%p\n",
			condition, C_se_condition);
}

void setSeSecondCondition(C_se_condition *C_se_condition, void *condition) {
	C_se_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for se condition at\t%p\n",
			condition, C_se_condition);
}
