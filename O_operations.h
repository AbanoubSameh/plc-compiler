/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * operations.h
 *
 *  Created on: Dec 24, 2018
 *      Author: abanoub
 */

#ifndef O_OPERATIONS_H_
#define O_OPERATIONS_H_

#include "parser.h"

typedef enum {
	E_direct_operation,
	E_assignment,

	E_addition,
	E_subtraction,
	E_multiplication,
	E_division,

	E_incrementation,
	E_decrementation,
} O_operation_type;

typedef struct O_operation {
	O_operation_type operation_type;
	void *operation;
} O_operation;

typedef struct O_direct_operation {
	O_operation_type operation_type;
	char *var;
} O_direct_operation;

typedef struct O_assignment {
	O_operation_type operation_type;
	struct O_operation *to;
	struct O_operation *from;
} O_assignment;

typedef struct O_plus {
	O_operation_type operation_type;
	struct O_operation *operation1;
	struct O_operation *operation2;
} O_plus;

typedef struct O_minus {
	O_operation_type operation_type;
	struct O_operation *operation1;
	struct O_operation *operation2;
} O_minus;

typedef struct O_times {
	O_operation_type operation_type;
	struct O_operation *operation1;
	struct O_operation *operation2;
} O_times;

typedef struct O_divide {
	O_operation_type operation_type;
	struct O_operation *operation1;
	struct O_operation *operation2;
} O_divide;

typedef struct O_increment {
	O_operation_type operation_type;
	char *var;
	bool type;
} O_increment;

typedef struct O_decrement {
	O_operation_type operation_type;
	char *var;
	bool type;
} O_decrement;

O_direct_operation* newO_direct_operation();
void setDirectOperation(O_direct_operation *O_direct_operation, char *string);

O_operation* newO_operation();
void setOperation(O_operation *O_operation, void *operation, O_operation_type type);

O_assignment* newO_assignment_operation();
void setAssignmentTO(O_assignment *O_assignment, void *operation);
void setAssignmentFROM(O_assignment *O_assignment, void *operation);

O_plus* newO_plus_operation();
void setPlusFirstOperation(O_plus *O_plus, void *operation);
void setPlusSecondOperation(O_plus *O_plus, void *operation);

O_minus* newO_minus_operation();
void setMinusFirstOperation(O_minus *O_minus, void *operation);
void setMinusSecondOperation(O_minus *O_minus, void *operation);

O_times* newO_times_operation();
void setTimesFirstOperation(O_times *O_times, void *operation);
void setTimesSecondOperation(O_times *O_times, void *operation);

O_divide* newO_divide_operation();
void setDivideFirstOperation(O_divide *O_divide, void *operation);
void setDivideSecondOperation(O_divide *O_divide, void *operation);

O_increment* newO_increment_operation();
void setIncrementOperation(O_increment *O_increment, char *string, bool type);

O_decrement* newO_decrement_operation();
void setDecrementOperation(O_decrement *O_decrement, char *string, bool type);

#endif /* O_OPERATIONS_H_ */
