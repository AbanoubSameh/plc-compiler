/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * code_generator.c
 *
 *  Created on: Dec 16, 2018
 *      Author: abanoub
 */

#include "G_conditions.h"
#include "G_operations.h"

int network = 1;

void G_function(C_function_declaration *function) {

	char *name = function->function_name;

	if (!strcmp(name, "output")) {
		name = "=";
	}

	printf("%s\t", name);
	fprintf(output, "%s", tabToSpace(name));

	C_node *temp = function->firstArgument;

	while (temp != NULL) {
		printf("%s", (char*) temp->value);
		fprintf(output, "%s", (char*) temp->value);
		if (temp->next != NULL) {
			printf(", ");
			fprintf(output, ", ");
		}
		temp = temp->next;
	}

	printf("\n");
	fprintf(output, "\n");

}

void G_comment(C_comment *comment) {

}

void G_if_statement(C_if_statement *statement) {
	G_condition(statement->condition, G_FIRST);
	code_generator(statement->body);
}

void code_generator(C_body *body) {

	C_node *temp = body->firstNode;

	while (temp != NULL) {
		if (temp->type == E_if_statement) {
			printf("Network %d\n", network);
			fprintf(output, "Network %d\n", network++);
			if (network == 2) {
				fseek(output, ftell(output) - 1, 0);
				fprintf(output, "%s", NETWORKONECOMMENT);
			}
			G_if_statement((C_if_statement*) temp->value);
		} else if (temp->type == E_function_declaration) {
			G_function((C_function_declaration*) temp->value);
		} else if (temp->type == E_comment) {
			G_comment((C_comment*) temp->value);
		} else if (temp->type == E_operation) {
			G_operation((O_operation*) temp->value);
		} else {
			printf("ignored node\n");
		}
		temp = temp->next;
	}

}

