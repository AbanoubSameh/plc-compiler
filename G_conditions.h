/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * G_conditions.h
 *
 *  Created on: Dec 16, 2018
 *      Author: abanoub
 */

#ifndef G_CONDITIONS_H_
#define G_CONDITIONS_H_

#include "parser.h"
#include "C_comparison_conditions.h"

#define G_FIRST		"LD"
#define G_AND		"A"
#define G_OR		"O"
#define G_NOT		"N"
#define G_POS		"EU"
#define G_NEG		"ED"
#define G_LDA		"ALD"
#define G_LDO		"OLD"
#define G_EQU		"="
#define G_NEQU		"<>"
#define G_GT		">"
#define G_ST		"<"
#define G_GTE		">="
#define G_STE		"<="
#define G_NOTCON	"NOT"

void G_and_condition(C_and_condition *and, char *toPrint);
void G_or_condition(C_or_condition *or, char *toPrint);
void G_direct_condition(C_direct_condition *direct, char *toPrint);
void G_not_condition(C_not_condition *not, char *toPrint);
void G_pos_condition(C_pos_condition *pos, char *toPrint);
void G_neg_condition(C_neg_condition *neg, char *toPrint);
void G_condition(C_condition *condition, char *toPrint);

void G_equal_condition(C_equal_condition *condition, char *toPrint);
void G_nequal_condition(C_nequal_condition *condition, char *toPrint);
void G_gthan_condition(C_gthan_condition *condition, char *toPrint);
void G_sthan_condition(C_sthan_condition *condition, char *toPrint);
void G_ge_condition(C_ge_condition *condition, char *toPrint);
void G_se_condition(C_se_condition *condition, char *toPrint);

#endif /* G_CONDITIONS_H_ */
