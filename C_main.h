/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_main.h
 *
 *  Created on: Dec 8, 2018
 *      Author: abanoub
 */

#ifndef C_MAIN_H_
#define C_MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int POINTERSIZE;// this variable should contain the pointer size on the machine
int ENUMSIZE;		// this variable should contain the enum size on the machine

struct C_body *abstract_syntax_tree;			// main program node

// enum used to know the type of struct that we are dealing with
typedef enum {
	E_abstract_syntax_tree,
	E_body,
	E_condition,
	E_function_declaration,
	E_if_statement,
	E_comment,
	E_string,
	E_operation
} C_type;

// enum used to know the type of condition we are dealing with
typedef enum {
	E_direct_condition,
	E_and_condition,
	E_or_condition,
	E_comparison,
	E_not_condition,
	E_pos_condition,
	E_neg_condition,
	E_equal_condition,
	E_nequal_condition,
	E_gthan_condition,
	E_sthan_condition,
	E_ge_condition,
	E_se_condition
} C_condition_type;

// node is like a liked list
typedef struct C_node {
	void *value;
	C_type type;
	struct C_node *next;
} C_node;

// abstract syntax tree for our program
typedef struct C_abstract_syntax_tree {
	C_type type;
	C_node *firstNode;
} C_abstract_syntax_tree;

// direct condition which contains a variable name
typedef struct C_direct_condition {
	C_condition_type condition_type;
	char *var;
} C_direct_condition;

// this structure contains two pointer for two conditions A && B
typedef struct C_and_condition {
	C_condition_type condition_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_and_condition;

// this structure contains two pointer for two conditions A || B
typedef struct C_or_condition {
	C_condition_type condition_type;
	struct C_condition *condition1;
	struct C_condition *condition2;
} C_or_condition;

// these structures contains a pointer to another condition
typedef struct C_not_condition {
	C_condition_type condition_type;
	void *condition;
} C_not_condition;

typedef struct C_pos_condition {
	C_condition_type condition_type;
	void *condition;
} C_pos_condition;

typedef struct C_neg_condition {
	C_condition_type condition_type;
	void *condition;
} C_neg_condition;

// this is the condition used by an if statement or while or ...
typedef struct C_condition {
	C_condition_type condition_type;
	void *condition;
} C_condition;

// this is the body of an if statement or a while or ...
typedef struct C_body {
	C_type type;
	C_node *firstNode;
} C_body;

// an if statement struct which should contain a condition and a body
typedef struct C_if_statement {
	C_type type;
	C_condition *condition;
	C_body *body;
} C_if_statement;

// a comment which only has a body
typedef struct C_comment {
	C_type type;
	C_body *body;
} C_comment;

// a function declaration like foo(a, b);
// it should contain the function name and all the arguments as nodes
typedef struct C_function_declaration {
	C_type type;
	char *function_name;
	C_node *firstArgument;
} C_function_declaration;

// these are the functions in each c file:

// these come from C_abstract_syntax_tree.c
C_abstract_syntax_tree *newC_abstract_syntax_tree();
void addTreeNode(C_abstract_syntax_tree *C_abstract_syntax_tree, C_node *node);

// these come from C_node.c
C_node *newC_node();
void setNodeValue(C_node *C_node, void *value, C_type type);
void setNodeNext(struct C_node *C_node, struct C_node *node);

// these come from C_body.c
C_body *newC_body();
void addBodyNode(C_body *C_body, C_node *node);

// these come from C_condition.c
C_direct_condition *newC_direct_condition();
void setDirectCondition(C_direct_condition *C_direct_condition, char *string);

C_and_condition *newC_and_condition();
void setAndFirstCondition(C_and_condition *C_and_condition, void *condition);
void setAndSecondCondition(C_and_condition *C_and_condition, void *condition);

C_or_condition *newC_or_condition();
void setOrFirstCondition(C_or_condition *C_or_condition, void *condition);
void setOrSecondCondition(C_or_condition *C_or_condition, void *condition);

C_not_condition *newC_not_condition();
void setNotCondition(C_not_condition *C_not_condition, void *condition);

C_pos_condition *newC_pos_condition();
void setPosCondition(C_pos_condition *C_pos_condition, void *condition);

C_neg_condition *newC_neg_condition();
void setNegCondition(C_neg_condition *C_neg_condition, void *condition);

C_condition *newC_condition();
void setCondition(C_condition *C_condition, void *condition,
		C_condition_type type);

// these come from C_if_statement.c
C_if_statement *newC_if_statement();
void setIfStatementCondition(C_if_statement *C_if_statement, void *condition);
void setIfStatementBody(C_if_statement *C_if_statement, void *body);

// these come from C_function_delaration.c
C_function_declaration *newC_function_declaration();
void setFunctionName(C_function_declaration *C_function_declaration,
		char *string);
void addFunctionArgument(C_function_declaration *C_function_declaration,
		C_node *node);

// function to store strings
char *newC_string(char *value);

// these come from C_comment.c
C_comment *newC_comment();
void setCommentBody(C_comment *C_comment, void *body);

#endif /* C_MAIN_H_ */
