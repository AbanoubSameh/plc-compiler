/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * compiler.c
 *
 *  Created on: Nov 30, 2018
 *      Author: abanoub
 */

#include "parser.h"

void plcCompiler() {
	printf("Parsing ...\n\n");

	abstract_syntax_tree = newC_body();
	parser(abstract_syntax_tree);

	printf("\nDone parsing\n\n");

	printf("%s", HEADER);
	fprintf(output, "%s", HEADER);
	code_generator(abstract_syntax_tree);
	printf("%s", FOOTER);
	fprintf(output, "%s", FOOTER);

	printf("\n\nDone compiling\n");

}

int main(int argc, char* argv[]) {

	char inputFileName[FILENAMELENGTH];
	char outputFileName[FILENAMELENGTH];
	char *dot;
	POINTERSIZE = sizeof(void*);
	ENUMSIZE = sizeof(C_type);
	lineCount = 1;

	if (argc == 1) {
		printf("please enter file name\n");
		fgets(inputFileName, 30, stdin);
		inputFileName[strlen(inputFileName) - 1] = 0;
	} else if (argc > 2) {
		printf("too many arguments\n");
		return -1;
	} else {
		if (strlen(argv[1]) > 30) {
			printf("file name is too long\n");
			return -1;
		} else {
			strcpy(inputFileName, argv[1]);
		}
	}

	printf("Opening %s\n\n", inputFileName);
	input = fopen(inputFileName, "r");

	if (input) {
		//	strcpy(inputFileName, "program.txt");

		dot = strchr(inputFileName, DOT);

		if (dot != NULL) {
			memcpy(outputFileName, inputFileName, (int) (dot - inputFileName));
			outputFileName[(int) (dot - inputFileName)] = 0;
		} else {
			strcpy(outputFileName, inputFileName);
		}

		strcat(outputFileName, EXTENSION);
//		printf("output file name: %s\n", outputFileName);

		output = fopen(outputFileName, "w");
		if (!output) {
			fprintf(output, "Hello World!\n");
		}

		plcCompiler();

		fclose(input);
		fclose(output);

	} else {
		printf("could not open file\n");
	}

}

