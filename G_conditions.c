/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * G_conditions.c
 *
 *  Created on: Dec 16, 2018
 *      Author: abanoub
 */

#include "G_conditions.h"

void G_equal_condition(C_equal_condition *condition, char *toPrint) {
	char *first = ((C_direct_condition*) condition->condition1)->var;
	char *second = ((C_direct_condition*) condition->condition2)->var;
	printf("%s%c%s\t%s, %s\n", toPrint, condition->var_type, G_EQU, first,
			second);
	fprintf(output, "%s%s, %s\n",
			tabToSpaceSCS(toPrint, condition->var_type, G_EQU), first, second);
}

void G_nequal_condition(C_nequal_condition *condition, char *toPrint) {
	char *first = ((C_direct_condition*) condition->condition1)->var;
	char *second = ((C_direct_condition*) condition->condition2)->var;
	printf("%s%c%s\t%s, %s\n", toPrint, condition->var_type, G_NEQU, first,
			second);
	fprintf(output, "%s%s, %s\n",
			tabToSpaceSCS(toPrint, condition->var_type, G_NEQU), first, second);
}

void G_gthan_condition(C_gthan_condition *condition, char *toPrint) {

	char *first = ((C_direct_condition*) condition->condition1)->var;
	char *second = ((C_direct_condition*) condition->condition2)->var;
	printf("%s%c%s\t%s, %s\n", toPrint, condition->var_type, G_GT, first,
			second);
	fprintf(output, "%s%s, %s\n",
			tabToSpaceSCS(toPrint, condition->var_type, G_GT), first, second);
}

void G_sthan_condition(C_sthan_condition *condition, char *toPrint) {

	char *first = ((C_direct_condition*) condition->condition1)->var;
	char *second = ((C_direct_condition*) condition->condition2)->var;
	printf("%s%c%s\t%s, %s\n", toPrint, condition->var_type, G_ST, first,
			second);
	fprintf(output, "%s%s, %s\n",
			tabToSpaceSCS(toPrint, condition->var_type, G_ST), first, second);
}

void G_ge_condition(C_ge_condition *condition, char *toPrint) {

	char *first = ((C_direct_condition*) condition->condition1)->var;
	char *second = ((C_direct_condition*) condition->condition2)->var;
	printf("%s%c%s\t%s, %s\n", toPrint, condition->var_type, G_GTE, first,
			second);
	fprintf(output, "%s%s, %s\n",
			tabToSpaceSCS(toPrint, condition->var_type, G_GTE), first, second);
}

void G_se_condition(C_se_condition *condition, char *toPrint) {

	char *first = ((C_direct_condition*) condition->condition1)->var;
	char *second = ((C_direct_condition*) condition->condition2)->var;
	printf("%s%c%s\t%s, %s\n", toPrint, condition->var_type, G_STE, first,
			second);
	fprintf(output, "%s%s, %s\n",
			tabToSpaceSCS(toPrint, condition->var_type, G_STE), first, second);
}

void G_and_condition(C_and_condition *and, char *toPrint) {

	G_condition(and->condition1, toPrint);

	if (bracketsCheck((C_condition*) and)) {
		G_condition(and->condition2, G_FIRST);
		printf("%s\n", G_LDA);
		fprintf(output, "%s\n", G_LDA);
	} else {
		G_condition(and->condition2, G_AND);
	}

}

void G_or_condition(C_or_condition *or, char *toPrint) {

	G_condition(or->condition1, toPrint);

	if (bracketsCheck((C_condition*) or)) {
		G_condition(or->condition2, G_FIRST);
		printf("%s\n", G_LDO);
		fprintf(output, "%s\n", G_LDO);
	} else {
		G_condition(or->condition2, G_OR);
	}

}

void G_not_condition(C_not_condition *not, char *toPrint) {

	if (((C_condition*) not->condition)->condition_type == E_direct_condition) {
		char name[4];
		memset(name, 0, 4);

		name[0] = toPrint[0];
		if (isFirstConnection(toPrint)) {
			name[1] = toPrint[1];
			name[2] = G_NOT[0];
		} else {
			name[1] = G_NOT[0];
		}

		G_condition(not->condition, name);
	} else {
		G_condition(not->condition, toPrint);
		printf("%s\n", G_NOTCON);
	}

}

void G_pos_condition(C_pos_condition *pos, char *toPrint) {
	G_condition(pos->condition, toPrint);
	printf("%s\n", G_POS);
	fprintf(output, "%s\n", G_POS);
}

void G_neg_condition(C_neg_condition *neg, char *toPrint) {
	G_condition(neg->condition, toPrint);
	printf("%s\n", G_NEG);
	fprintf(output, "%s\n", G_NEG);
}

void G_direct_condition(C_direct_condition *direct, char *toPrint) {
	printf("%s\t", toPrint);
	fprintf(output, "%s", tabToSpace(toPrint));
	printf("%s\n", direct->var);
	fprintf(output, "%s\n", direct->var);
}

void G_condition(C_condition *condition, char *toPrint) {

	if (condition->condition_type == E_direct_condition) {
		G_direct_condition((C_direct_condition*) condition, toPrint);
	} else if (condition->condition_type == E_and_condition) {
		G_and_condition((C_and_condition*) condition, toPrint);
	} else if (condition->condition_type == E_or_condition) {
		G_or_condition((C_or_condition*) condition, toPrint);
	} else if (condition->condition_type == E_not_condition) {
		G_not_condition((C_not_condition*) condition, toPrint);
	} else if (condition->condition_type == E_pos_condition) {
		G_pos_condition((C_pos_condition*) condition, toPrint);
	} else if (condition->condition_type == E_neg_condition) {
		G_neg_condition((C_neg_condition*) condition, toPrint);
	} else if (condition->condition_type == E_equal_condition) {
		G_equal_condition((C_equal_condition*) condition, toPrint);
	} else if (condition->condition_type == E_nequal_condition) {
		G_nequal_condition((C_nequal_condition*) condition, toPrint);
	} else if (condition->condition_type == E_gthan_condition) {
		G_gthan_condition((C_gthan_condition*) condition, toPrint);
	} else if (condition->condition_type == E_sthan_condition) {
		G_sthan_condition((C_sthan_condition*) condition, toPrint);
	} else if (condition->condition_type == E_ge_condition) {
		G_ge_condition((C_ge_condition*) condition, toPrint);
	} else if (condition->condition_type == E_se_condition) {
		G_se_condition((C_se_condition*) condition, toPrint);
	} else if (condition->condition_type == E_neg_condition) {
		G_neg_condition((C_neg_condition*) condition, toPrint);
	} else {
		printf("ignored condition\n");
	}

}

