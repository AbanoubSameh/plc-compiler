/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * operations.c
 *
 *  Created on: Dec 24, 2018
 *      Author: abanoub
 */

#include "O_operations.h"

O_direct_operation* newO_direct_operation() {
	O_direct_operation *newO_direct_operation = malloc(ENUMSIZE + POINTERSIZE);
	newO_direct_operation->operation_type = E_direct_operation;
	newO_direct_operation->var = NULL;
	printf("new direct operation created at:\t%p\n", newO_direct_operation);
	return newO_direct_operation;
}

void setDirectOperation(O_direct_operation *O_direct_operation, char *string) {
	char *var = malloc(strlen(string) + 1);
	strcpy(var, string);
	O_direct_operation->var = var;
	printf("string with value:\t%s\tset as var for direct operation at\t%p\n",
			string, O_direct_operation);
}

O_operation* newO_operation() {
	O_operation *newO_operation = malloc(ENUMSIZE + POINTERSIZE);
	newO_operation->operation_type = E_operation;
	newO_operation->operation = NULL;
	printf("new operation created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setOperation(O_operation *O_operation, void *operation, O_operation_type type) {
	O_operation->operation = operation;
	O_operation->operation_type = E_operation;
	printf("operation at\t%p\tset as operation for operation at\t%p\twith type\t%d\n",
			operation, O_operation, type);
}

O_assignment* newO_assignment_operation() {
	O_assignment *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_assignment;
	newO_operation->to = NULL;
	newO_operation->from = NULL;
	printf("new assignment created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setAssignmentTO(O_assignment *O_assignment, void *operation) {
	O_assignment->to = operation;
	printf("operation at\t%p\tset as to operation for assignment operation at\t%p\n",
			operation, O_assignment);
}

void setAssignmentFROM(O_assignment *O_assignment, void *operation) {
	O_assignment->from = operation;
	printf("operation at\t%p\tset as from operation for assignment operation at\t%p\n",
			operation, O_assignment);
}

O_plus* newO_plus_operation() {
	O_plus *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_addition;
	newO_operation->operation1 = NULL;
	newO_operation->operation1 = NULL;
	printf("new addition created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setPlusFirstOperation(O_plus *O_plus, void *operation) {
	O_plus->operation1 = operation;
	printf("operation at\t%p\tset as first operation for plus operation at\t%p\n",
			operation, O_plus);
}

void setPlusSecondOperation(O_plus *O_plus, void *operation) {
	O_plus->operation2 = operation;
	printf(
			"operation at\t%p\tset as second operation for plus operation at\t%p\n",
			operation, O_plus);
}

O_minus* newO_minus_operation() {
	O_minus *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_subtraction;
	newO_operation->operation1 = NULL;
	newO_operation->operation1 = NULL;
	printf("new subtraction created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setMinusFirstOperation(O_minus *O_minus, void *operation) {
	O_minus->operation1 = operation;
	printf("operation at\t%p\tset as first operation for minus operation at\t%p\n",
			operation, O_minus);
}

void setMinusSecondOperation(O_minus *O_minus, void *operation) {
	O_minus->operation2 = operation;
	printf(
			"operation at\t%p\tset as second operation for minus operation at\t%p\n",
			operation, O_minus);
}

O_times* newO_times_operation() {
	O_times *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_multiplication;
	newO_operation->operation1 = NULL;
	newO_operation->operation1 = NULL;
	printf("new multiplication created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setTimesFirstOperation(O_times *O_times, void *operation) {
	O_times->operation1 = operation;
	printf("operation at\t%p\tset as first operation for times operation at\t%p\n",
			operation, O_times);
}

void setTimesSecondOperation(O_times *O_times, void *operation) {
	O_times->operation2 = operation;
	printf(
			"operation at\t%p\tset as second operation for times operation at\t%p\n",
			operation, O_times);
}

O_divide* newO_divide_operation() {
	O_divide *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_division;
	newO_operation->operation1 = NULL;
	newO_operation->operation1 = NULL;
	printf("new division created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setDivideFirstOperation(O_divide *O_divide, void *operation) {
	O_divide->operation1 = operation;
	printf("operation at\t%p\tset as first operation for divide operation at\t%p\n",
			operation, O_divide);
}

void setDivideSecondOperation(O_divide *O_divide, void *operation) {
	O_divide->operation2 = operation;
	printf(
			"operation at\t%p\tset as second operation for divide operation at\t%p\n",
			operation, O_divide);
}

O_increment* newO_increment_operation() {
	O_increment *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_incrementation;
	newO_operation->var = NULL;
	newO_operation->type = NULL;
	printf("new incrementation created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setIncrementOperation(O_increment *O_increment, char *string, bool type) {
	char *var = malloc(strlen(string) + 1);
	strcpy(var, string);
	O_increment->var = var;
	O_increment->type = type;
	printf("string with value:\t%s\tset as var for increment operation at\t%p with type %d\n",
			string, O_increment, type);
}

O_decrement* newO_decrement_operation() {
	O_decrement *newO_operation = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newO_operation->operation_type = E_decrementation;
	newO_operation->var = NULL;
	newO_operation->type = NULL;
	printf("new decrementation created at:\t%p\n", newO_operation);
	return newO_operation;
}

void setDecrementOperation(O_decrement *O_decrement, char *string, bool type) {
	char *var = malloc(strlen(string) + 1);
	strcpy(var, string);
	O_decrement->var = var;
	O_decrement->type = type;
	printf("string with value:\t%s\tset as var for decrement operation at\t%p with type %d\n",
			string, O_decrement, type);
}
