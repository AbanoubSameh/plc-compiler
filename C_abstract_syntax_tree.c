/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_abstract_syntax_tree.c
 *
 *  Created on: Dec 8, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_abstract_syntax_tree *newC_abstract_syntax_tree() {
	C_abstract_syntax_tree *newC_abstract_syntax_tree = malloc(
			ENUMSIZE + POINTERSIZE);
	newC_abstract_syntax_tree->type = 0;
	newC_abstract_syntax_tree->firstNode = NULL;
	printf("new a.s.t. created at:\t\t%p\n", newC_abstract_syntax_tree);
	return newC_abstract_syntax_tree;
}

void addTreeNode(C_abstract_syntax_tree *C_abstract_syntax_tree, C_node *node) {
	if (C_abstract_syntax_tree->firstNode == NULL) {
		C_abstract_syntax_tree->firstNode = node;
		printf("a.s.t. at\t%p\t does not have a first node\n",
				C_abstract_syntax_tree);
		printf("node at\t%p\tset as first node for a.s.t. at\t%p", node,
				C_abstract_syntax_tree);
	} else {
		C_node *temp = C_abstract_syntax_tree->firstNode;
		while (temp->next != NULL) {
			temp = temp->next;
		}
		temp->next = node;
		printf("a.s.t. at\t%p\t has a first node\n", C_abstract_syntax_tree);
		printf("node at\t%p\tset as node for a.s.t. at\t%p", node,
				C_abstract_syntax_tree);
	}
}


