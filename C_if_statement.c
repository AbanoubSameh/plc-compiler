/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * if_statement.c
 *
 *  Created on: Dec 8, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_if_statement *newC_if_statement() {
	C_if_statement *newC_if_statement = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newC_if_statement->type = E_if_statement;
	newC_if_statement->condition = NULL;
	newC_if_statement->body = NULL;
	printf("new if statement created at:\t%p\n", newC_if_statement);
	return newC_if_statement;
}

void setIfStatementCondition(C_if_statement *C_if_statement, void *condition) {
	C_if_statement->condition = condition;
	printf("condition at\t%p\tset as condition for if statement at\t%p\n",
			condition, C_if_statement);
}

void setIfStatementBody(C_if_statement *C_if_statement, void *body) {
	C_if_statement->body = body;
	printf("body at\t%p\tset as body for if statement at\t%p\n", body,
			C_if_statement);
}
