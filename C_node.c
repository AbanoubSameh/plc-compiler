/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_node.c
 *
 *  Created on: Dec 9, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_node *newC_node() {
	C_node *newC_node = malloc(ENUMSIZE + 2 * POINTERSIZE);
	printf("new node created at:\t%p\n", newC_node);
	newC_node->next = NULL;
	newC_node->value = NULL;
	return newC_node;
}

void setNodeValue(C_node *C_node, void *value, C_type type) {
	C_node->value = value;
	C_node->type = type;
	printf("value at\t%p\tset as value for node at\t%p\twith type\t%d\n", value,
			C_node, type);
}

void setNodeNext(struct C_node *C_node, struct C_node *node) {
	C_node->next = node;
	printf("node at\t%p\tset as next for node at\t%p\n", node, C_node);
}
