/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * lexer.c
 *
 *  Created on: Dec 1, 2018
 *      Author: abanoub
 */

#include "parser.h"

// this function returns a token by reading a file and grouping together a string,
// an operator or a symbol ...
int lexer(char* line) {

	int lineIndex = 0;
	char read;

	enum stateEnum {
		normal, sign
	} state = normal;

	memset(line, 0, strlen(line));

	while ((read = fgetc(input)) != EOF) {

		if (read == 10) {
			lineCount++;
		}

		if (state == sign) {
			if ((read == line[0] && read != 42 && read != 33) || read == 61) {
				line[1] = read;
				if (read == 10) {
					lineCount++;
				}
				return 1;
			} else {
				fseek(input, ftell(input) - 1, 0);
				if (read == 10) {
					lineCount++;
				}
				return 1;
			}
		} else if ((read >= 65 && read <= 90) || (read >= 97 && read <= 122)
				|| (read >= 48 && read <= 57) || read == 46 || read == 35) {
			line[lineIndex] = read;
			lineIndex++;
		} else if (read == 40 || read == 41 || read == 123 || read == 125
				|| read == 59 || read == 44 || read == 34 || read == 39
				|| read == 10) {
			if (strlen(line) == 0) {
				line[0] = read;
			} else {
				fseek(input, ftell(input) - 1, 0);
			}
			if (read == 10) {
				lineCount++;
			}
			return 1;
		} else if (read == 43 || read == 45 || read == 42 || read == 47
				|| read == 61 || read == 60 || read == 62 || read == 33) {
			if (strlen(line) == 0) {
				state = sign;
				line[0] = read;
			} else {
				fseek(input, ftell(input) - 1, 0);
				if (read == 10) {
					lineCount++;
				}
				return 1;
			}
		} else if (read == 38 || read == 124) {
			if (strlen(line) == 0) {
				line[0] = read;
			} else if (strlen(line) == 1 && line[0] == read) {
				line[1] = read;
				if (read == 10) {
					lineCount++;
				}
				return 1;
			} else {
				fseek(input, ftell(input) - 1, 0);
				if (read == 10) {
					lineCount++;
				}
				return 1;
			}
		} else if (read == 92) {
			line[lineIndex] = read;
			line[++lineIndex] = fgetc(input);
		} else {
			if (strlen(line) == 0) {
				continue;
			}
			if (read == 10) {
				lineCount++;
			}
			return 1;
		}

	}

	if (read == 10) {
		lineCount++;
	}
	return 0;

}

