/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_comment.c
 *
 *  Created on: Dec 14, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_comment *newC_comment() {
	C_comment *newC_comment = malloc(ENUMSIZE + POINTERSIZE);
	newC_comment->type = E_comment;
	newC_comment->body = NULL;
	printf("new comment created at:\t%p\n", newC_comment);
	return newC_comment;
}

void setCommentBody(C_comment *C_comment, void *body) {
	C_comment->body = body;
	printf("body at\t%p\tset as body for comment at\t%p\n", body, C_comment);
}
