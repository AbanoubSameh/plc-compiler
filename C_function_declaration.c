/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_function.c
 *
 *  Created on: Dec 8, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_function_declaration *newC_function_declaration() {
	C_function_declaration *newC_function_declaration = malloc(
			ENUMSIZE + 2 * POINTERSIZE);
	newC_function_declaration->type = E_function_declaration;
	newC_function_declaration->firstArgument = NULL;
	newC_function_declaration->function_name = NULL;
	printf("new function declaration created at:\t%p\n",
			newC_function_declaration);
	return newC_function_declaration;
}

void setFunctionName(C_function_declaration *C_function_declaration,
		char *string) {
	char *var = malloc(strlen(string) + 1);
	strcpy(var, string);
	C_function_declaration->function_name = var;
	printf(
			"string at\t%p\t set as function name for function declaration at\t%p\n",
			string, C_function_declaration);
}

void addFunctionArgument(C_function_declaration *C_function_declaration,
		C_node *node) {
	if (C_function_declaration->firstArgument == NULL) {
		C_function_declaration->firstArgument = node;
		printf("function declaration at\t%p\tdoes not have a first node\n",
				C_function_declaration);
		printf(
				"argument at\t%p\t set as first argument for function declaration at\t%p\n",
				node, C_function_declaration);
	} else {
		C_node *temp = C_function_declaration->firstArgument;
		while (temp->next != NULL) {
			temp = temp->next;
		}
		temp->next = node;
		printf("function declaration at\t%p\thas a first node\n",
				C_function_declaration);
		printf(
				"argument at\t%p\t set as new argument for function declaration at\t%p\n",
				node, C_function_declaration);
	}

}
