/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * variables.c
 *
 *  Created on: Dec 25, 2018
 *      Author: abanoub
 */

#include "parser.h"

O_operation* makeNewOperation(char *op, O_operation *op1, O_operation *op2) {

	O_operation *toReturn = NULL;

	if (isAdditionSign(op)) {
		O_plus *operation = newO_plus_operation();
		toReturn = (O_operation*) operation;
		setPlusFirstOperation(operation, (O_operation*) op1);
		setPlusSecondOperation(operation, op2);
	} else if (isSubtractionSign(op)) {
		O_minus *operation = newO_minus_operation();
		toReturn = (O_operation*) operation;
		setMinusFirstOperation(operation, (O_operation*) op1);
		setMinusSecondOperation(operation, op2);
	} else if (isMultiplicationSign(op)) {
		O_times *operation = newO_times_operation();
		toReturn = (O_operation*) operation;
		setTimesFirstOperation(operation, (O_operation*) op1);
		setTimesSecondOperation(operation, op2);
	} else if (isDivisionSign(op)) {
		O_divide *operation = newO_divide_operation();
		toReturn = (O_operation*) operation;
		setDivideFirstOperation(operation, (O_operation*) op1);
		setDivideSecondOperation(operation, op2);
	} else {
		syntax_error();
	}

	return toReturn;

}

O_operation* solveIncAndDec(char *operation, char *variable, bool type) {

	if (isMemoryLocation(variable)) {
		if (isIncOp(operation)) {
			O_increment *op = newO_increment_operation();
			setIncrementOperation(op, variable, type);
			return (O_operation*) op;
		} else if (isDecOp(operation)) {
			O_decrement *op = newO_decrement_operation();
			setDecrementOperation(op, variable, type);
			return (O_operation*) op;
		} else {
			syntax_error();
		}
	} else {
		syntax_error();
	}

	return NULL;
}

O_operation* solveMulAndDiv() {

	O_operation *firstOP = NULL;

	char last[TOKENLENGTH];
	strcpy(last, token);
	lexer(token);

	printf("token .... : %s\n", token);

	if (isIncOp(last) || isDecOp(last)) {
		if (isMemoryLocation(token)) {
			firstOP = solveIncAndDec(last, token, true);
			lexer(token);
		} else {
			syntax_error();
		}
	} else if (isORB(last)) {
		firstOP = variable();
		if (!isCRB(token)) {
			syntax_error();
		} else {
			lexer(token);
		}
	} else if ((isIncOp(token) || isDecOp(token))) {
		if (isMemoryLocation(last)) {
			firstOP = solveIncAndDec(token, last, false);
			lexer(token);
		} else {
			syntax_error();
		}
	}

	if (firstOP == NULL) {
		O_direct_operation *op = newO_direct_operation();
		setDirectOperation(op, last);
		firstOP = (O_operation*) op;
	}

	return firstOP;

}

O_operation* variable() {

	O_operation *toReturn = NULL;
	O_operation *firstOP = NULL;
	O_operation *secondOP = NULL;

	char last[TOKENLENGTH];
	strcpy(last, token);
	lexer(token);

	printf("token .... : %s\n", token);

	if (isIncOp(last) || isDecOp(last)) {
		if (isMemoryLocation(token)) {
			firstOP = solveIncAndDec(last, token, true);
			lexer(token);
		} else {
			syntax_error();
		}
	} else if (isORB(last)) {
		firstOP = variable();
		if (!isCRB(token)) {
			syntax_error();
		} else {
			lexer(token);
		}
	} else if ((isIncOp(token) || isDecOp(token))) {
		if (isMemoryLocation(last)) {
			firstOP = solveIncAndDec(token, last, false);
			lexer(token);
		} else {
			syntax_error();
		}
	}

	if (firstOP == NULL) {
		O_direct_operation *op = newO_direct_operation();
		setDirectOperation(op, last);
		firstOP = (O_operation*) op;
	}

	printf("token .... : %s\n", token);

	while (true) {
		if (isSemicolon(token) || isCRB(token)) {
			return firstOP;
		} else if (isAdditionSign(token) || isSubtractionSign(token)) {
			strcpy(last, token);
			lexer(token);
			secondOP = variable();
			toReturn = makeNewOperation(last, firstOP, secondOP);
			break;
		} else if (isMultiplicationSign(token) || isDivisionSign(token)) {

			while (isMultiplicationSign(token) || isDivisionSign(token)) {
				strcpy(last, token);
				lexer(token);
				secondOP = solveMulAndDiv();
				firstOP = makeNewOperation(last, firstOP, secondOP);
			}

		} else if (isSpace(token)) {
			lexer(token);
		} else {
			syntax_error();
		}
	}

	return toReturn;
}

void assignVariable(C_body *body, char *name) {

	do {

		if (isSpace(name)) {
			continue;
		} else if (isEqualSign(token) || isPlusAssign(token)
				|| isMinusAssign(token) || isTimesAssign(token)
				|| isDivideAssign(token)) {

			lexer(token);

			O_assignment *assign = newO_assignment_operation();

			O_direct_operation *op = newO_direct_operation();
			setDirectOperation(op, name);

			setAssignmentTO(assign, (O_operation*) op);
			setAssignmentFROM(assign, variable());

			C_node *newNode = newC_node();
			setNodeValue(newNode, (void*) assign, E_operation);
			addBodyNode(body, newNode);

			break;
		} else if (isSemicolon(token)) {
			printf("empty assignment\n");
			syntax_error();
			break;
		} else {
			printf("token: %s\n", token);
			syntax_error();
		}

	} while (lexer(token));

}

