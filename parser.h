/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * parser.h
 *
 *  Created on: Dec 2, 2018
 *      Author: abanoub
 */

#ifndef PARSER_H_
#define PARSER_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "C_main.h"
#include "definitions.h"	// this file contains ascii definitions and token definitions
#include "O_operations.h"

typedef struct node {
	char *string;
	struct node* next;
} node;

#define TOKENLENGTH 40		// maximum token length
#define FILENAMELENGTH 40	// maximum file name
#define TABSIZE 7			// tab size for output file

int lineCount;				// file line count
char token[TOKENLENGTH];	// current token
char tabTemp[TABSIZE];		// temporary storage for some output

FILE *input;				// input file
FILE *output;				// output file

struct node *newNode(char *string);		// returns a new linkedList node
void printList(node *root);				// prints a linkedList
void cleanList(node *root);				// frees the memory of a linkedList

int lexer(char* line);					// lexer is used to read a file and return a token
char escape(char esc);					// escape sequences for comments

// these come from the functions.c file and they are some common functions
bool isVariable(char *token);
bool isComparisonOperator(char *token);
bool isAnd(char *token);
bool isOr(char *token);
bool isNot(char *token);
bool isPos(char *token);
bool isNeg(char *token);
bool isNewLine(char *token);
bool isSpace(char *token);
bool isORB(char *token);
bool isCRB(char *token);
bool isOSB(char *token);
bool isCSB(char *token);
bool isOCB(char *token);
bool isCCB(char *token);
bool isComment(char *token);
bool isSemicolon(char *token);
bool isComma(char *token);
bool isEqualSign(char *token);
bool isExclamationMark(char *token);
bool isFirstConnection(char *connection);
bool isEqual(char *token);
bool isNE(char *token);
bool isGE(char *token);
bool isSE(char *token);
bool isGT(char *token);
bool isST(char *token);
bool isPlusAssign(char *token);
bool isMinusAssign(char *token);
bool isTimesAssign(char *token);
bool isDivideAssign(char *token);
bool isIncOp(char *token);
bool isDecOp(char *token);
bool isSHROp(char *token);
bool isSHLOp(char *token);
bool isAdditionSign(char *token);
bool isSubtractionSign(char *token);
bool isMultiplicationSign(char *token);
bool isDivisionSign(char *token);
bool isOperation(char *token);
bool isMemoryLocation(char *variable);
char* tabToSpace(char *word);
char* tabToSpaceSCS(char *word, char letter, char *word2);
char getVariableType(char *variable);
char varType(char *var1, char *var2);

// these two are very very complicated stuff that should be never messed with
bool bracketsCheck(C_condition *condition);
void checkComparisonVariable(C_condition *condition);

// these are the types of language structures we are looking for -for now-
void parser(C_body *body);
void if_statement(C_body *body);
void function(C_body *body);
void comment(C_body *body);
void assignVariable(C_body *body, char *name);
struct O_operation* solveIncAndDec(char *operation, char *variable, bool type);
struct O_operation* variable();

// this is the function from the code_generator.c file
void code_generator(C_body *body);

// error handling, these come from errors.c
void error();
void syntax_error();
void arithmetic_error();
void variable_type_error();
void full_registers_error();

#endif /* PARSER_H_ */
