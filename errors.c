/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * errors.c
 *
 *  Created on: Dec 2, 2018
 *      Author: abanoub
 */

#include "parser.h"

// this file is used for error handling, it should to be expanded as needed
void end() {
	printf("exiting ...\n");
	exit(-1);
}

void error() {
	printf("an error occurred at line %d\n", lineCount);
	end();
}

void syntax_error() {
	printf("syntax error occurred at line %d\n", lineCount);
	end();
}

void arithmetic_error() {
	printf("arithmetic error occurred at line %d\n", lineCount);
	end();
}

void variable_type_error() {
	printf("incompatible variables at line %d\n", lineCount);
	end();
}

void full_registers_error() {
	printf("operation is too big%d\n", lineCount);
	end();
}
