/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * G_operations.h
 *
 *  Created on: Jan 11, 2019
 *      Author: abanoub
 */

#ifndef G_OPERATIONS_H_
#define G_OPERATIONS_H_

#include "parser.h"

#define	G_MUL		"MUL"
#define G_DIV		"DIV"
#define G_INC		"INC"
#define G_DEC		"DEC"
#define G_MOV		"MOV"
#define G_TO		"T"
#define G_BCD		"BCD"
#define G_ASCII		"A"
#define G_HEX		"H"
#define G_SQRT		"SQRT"
#define G_SIN		"SIN"
#define G_COS		"COS"
#define G_TAN		"TAN"
#define G_EXP		"EXP"
#define G_LOG		"LN"
#define G_ACC0		"AC0"
#define G_ACC1		"AC1"
#define G_ACC2		"AC2"
#define G_ACC3		"AC3"

char* G_operation(O_operation *operation);
char* G_direct_operation(O_direct_operation *operation);
void G_assignment(O_assignment *operation);
char* G_addition(O_plus *operation);
char* G_subtraction(O_minus *operation);
char* G_multiplication(O_times *operation);
char* G_division(O_divide *operation);
char* G_incrementation(O_increment *operation);
char* G_decrementation(O_decrement *operation);

#endif /* G_OPERATIONS_H_ */
