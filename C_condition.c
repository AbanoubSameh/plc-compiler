/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * C_condition.c
 *
 *  Created on: Dec 8, 2018
 *      Author: abanoub
 */

#include "C_main.h"

C_direct_condition *newC_direct_condition() {
	C_direct_condition *newC_direct_condition = malloc(ENUMSIZE + POINTERSIZE);
	newC_direct_condition->condition_type = E_direct_condition;
	newC_direct_condition->var = NULL;
	printf("new direct condition created at:\t%p\n", newC_direct_condition);
	return newC_direct_condition;
}

void setDirectCondition(C_direct_condition *C_direct_condition, char *string) {
	char *var = malloc(strlen(string) + 1);
	strcpy(var, string);
	C_direct_condition->var = var;
	printf("string with value:\t%s\tset as var for direct condition at\t%p\n",
			string, C_direct_condition);
}

C_and_condition *newC_and_condition() {
	C_and_condition *newC_and_condition = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newC_and_condition->condition_type = E_and_condition;
	newC_and_condition->condition1 = NULL;
	newC_and_condition->condition2 = NULL;
	printf("new and condition created at:\t%p\n", newC_and_condition);
	return newC_and_condition;
}

void setAndFirstCondition(C_and_condition *C_and_condition, void *condition) {
	C_and_condition->condition1 = condition;
	printf(
			"condition at\t%p\tset as first condition for and condition at\t%p\n",
			condition, C_and_condition);
}

void setAndSecondCondition(C_and_condition *C_and_condition, void *condition) {
	C_and_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for and condition at\t%p\n",
			condition, C_and_condition);
}

C_or_condition *newC_or_condition() {
	C_or_condition *newC_or_condition = malloc(ENUMSIZE + 2 * POINTERSIZE);
	newC_or_condition->condition_type = E_or_condition;
	newC_or_condition->condition1 = NULL;
	newC_or_condition->condition2 = NULL;
	printf("new or condition created at:\t%p\n", newC_or_condition);
	return newC_or_condition;
}

void setOrFirstCondition(C_or_condition *C_or_condition, void *condition) {
	C_or_condition->condition1 = condition;
	printf("condition at\t%p\tset as first condition for or condition at\t%p\n",
			condition, C_or_condition);
}

void setOrSecondCondition(C_or_condition *C_or_condition, void *condition) {
	C_or_condition->condition2 = condition;
	printf(
			"condition at\t%p\tset as second condition for or condition at\t%p\n",
			condition, C_or_condition);
}

C_not_condition *newC_not_condition() {
	C_not_condition *newC_not_condition = malloc(ENUMSIZE + POINTERSIZE);
	newC_not_condition->condition_type = E_not_condition;
	newC_not_condition->condition = NULL;
	printf("new not condition created at:\t%p\n", newC_not_condition);
	return newC_not_condition;
}

void setNotCondition(C_not_condition *C_not_condition, void *condition) {
	C_not_condition->condition = condition;
	printf("condition at\t%p\tset as condition for not condition at\t%p\n",
			condition, C_not_condition);
}

C_pos_condition *newC_pos_condition() {
	C_pos_condition *newC_pos_condition = malloc(ENUMSIZE + POINTERSIZE);
	newC_pos_condition->condition_type = E_pos_condition;
	newC_pos_condition->condition = NULL;
	printf("new pos condition created at:\t%p\n", newC_pos_condition);
	return newC_pos_condition;
}

void setPosCondition(C_pos_condition *C_pos_condition, void *condition) {
	C_pos_condition->condition = condition;
	printf("condition at\t%p\tset as condition for pos condition at\t%p\n",
			condition, C_pos_condition);
}

C_neg_condition *newC_neg_condition() {
	C_neg_condition *newC_neg_condition = malloc(ENUMSIZE + POINTERSIZE);
	newC_neg_condition->condition_type = E_neg_condition;
	newC_neg_condition->condition = NULL;
	printf("new neg condition created at:\t%p\n", newC_neg_condition);
	return newC_neg_condition;
}

void setNegCondition(C_neg_condition *C_neg_condition, void *condition) {
	C_neg_condition->condition = condition;
	printf("condition at\t%p\tset as condition for neg condition at\t%p\n",
			condition, C_neg_condition);
}

C_condition *newC_condition() {
	C_condition *newC_condition = malloc(ENUMSIZE + POINTERSIZE);
	newC_condition->condition = NULL;
	printf("new condition created at:\t%p\n", newC_condition);
	return newC_condition;
}

void setCondition(C_condition *C_condition, void *condition,
		C_condition_type type) {
	C_condition->condition = condition;
	C_condition->condition_type = E_condition;
	printf(
			"condition at\t%p\tset as condition for condition at\t%p\twith type\t%d\n",
			condition, C_condition, type);
}

