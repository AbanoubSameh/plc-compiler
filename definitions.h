/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * definitions.h
 *
 *  Created on: Dec 7, 2018
 *      Author: abanoub
 */

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

// ascii characters
#define TAB					9
#define NEWLINE				10
#define SPACE				32
#define EXCLAMATIONMARK		33
#define DOUBLEQUOTE			34
#define NUMBERSIGN			35
#define DOLLARSIGN			36
#define PERCENT				37
#define AMPERSAND			38
#define QUATE				39
#define OROUNDBRACKET		40
#define CROUNDBRACKET		41
#define ASTERISK			42
#define PLUSSIGN			43
#define COMMA				44
#define MINUSSIGN			45
#define DOT					46
#define FORWARDSLASH		47
#define ZERO				48
#define NINE				57
#define COLON				58
#define SEMICOLON			59
#define SMALLERTHAN			60
#define EQUALSIGN			61
#define GREATERTHAN			62
#define QUESTIONMARK		63
#define ATSYMBOL			64
#define UPPERCASEA			65
#define UPPERCASEZ			90
#define OSQUAREBRACKET		91
#define BACKSLASH			92
#define CSQUAREBRACKET		93
#define CARET				94
#define UNDERSCORE			95
#define GRAVEACCENT			96
#define LOWERCASEA			97
#define LOWERCASEZ			122
#define OCURLYBRACKET		123
#define VERTICALBAR			124
#define CCURLYBRACKET		125
#define	TILDE				126

// tokens
#define IF					"if"
#define POS					"POS"
#define NEG					"NEG"
#define COMMENT				"//"
#define AND					"&&"
#define OR					"||"
#define EQUAL				"=="
#define GE					">="
#define SE					"<="
#define NE					"!="
#define PLUSE				"+="
#define MINUSE				"-="
#define TIMESE				"*="
#define DIVIDEE				"/="
#define INC					"++"
#define DEC					"--"
#define SHR					">>"
#define SHL					"<<"

// colors
#define BLACK  				"\x1B[30m"
#define RED   				"\x1B[31m"
#define GREEN   			"\x1B[32m"
#define YELLOW   			"\x1B[33m"
#define BLUE				"\x1B[34m"
#define MAGENTA				"\x1B[35m"
#define CYAN				"\x1B[36m"
#define WHITE				"\x1B[37m"
#define RESET 				"\x1B[0m"

// variable types
#define BYTE				"B"
#define WORD				"W"
#define DOUBLE				"D"

#define INTEGER				"I"
#define FLOAT				"F"
#define REAL				"R"

// prefixes
#define BINARAY				"0b"
#define OCTAL				"0"
#define HEX					"0x"

// program extension
#define EXTENSION ".awl"

// useless stuff to add in every program
#define HEADER "ORGANIZATION_BLOCK MAIN:OB1\n\
TITLE=PROGRAM COMMENTS\n\
BEGIN\n"

#define FOOTER "END_ORGANIZATION_BLOCK\n\
SUBROUTINE_BLOCK SBR_0:SBR0\n\
TITLE=SUBROUTINE COMMENTS\n\
BEGIN\n\
Network 1 // Network Title\n\
// Network Comment\n\
END_SUBROUTINE_BLOCK\n\
INTERRUPT_BLOCK INT_0:INT0\n\
TITLE=INTERRUPT ROUTINE COMMENTS\n\
BEGIN\n\
Network 1 // Network Title\n\
// Network Comment\n\
END_INTERRUPT_BLOCK\n"

#define NETWORKONECOMMENT " // Network Title\n\
// Network Comment\n"

#endif /* DEFINITIONS_H_ */
