/*
 * Copyright (C) 2018  Abanoub Sameh
 *
 * This file is part of plc-compiler.
 *
 * plc-compiler is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * plc-compiler is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with plc-compiler.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * parser.c
 *
 *  Created on: Dec 2, 2018
 *      Author: abanoub
 */

#include "parser.h"
#include "conditions_parser.h"

void parser(C_body *body) {

	while (lexer(token) != 0) {
//		printf("token: %s\n", token);

		if (!strcmp(token, IF)) {
			if_statement(body);
		} else if (isComment(token)) {
			comment(body);
		} else if (isSpace(token)) {
			continue;
		} else if (isCCB(token)) {
			return;
		} else {
			function(body);
		}
	}

}

void if_statement(C_body *body) {

	printf("if_statement:\n");
	C_if_statement *statement = newC_if_statement();

	if (isORB(token) && isSpace(token)) {
		syntax_error();
	}
	lexer(token);
	setIfStatementCondition(statement, condition(input));

	lexer(token);

	if (isOCB(token) && isSpace(token)) {
		syntax_error();
	}

	printf("if_statement_body\n");
	C_body *statement_body = newC_body();
	setIfStatementBody(statement, statement_body);

	while (lexer(token) != 0 && !isCCB(token)) {
		parser(statement_body);
	}

	C_node *newNode = newC_node();
	setNodeValue(newNode, (void*) statement, E_if_statement);
	addBodyNode(body, newNode);

}

void comment(C_body *body) {
	printf("comment:\n");
	printf("//");
	/*
	 C_comment *comment = newC_comment();
	 C_body *commentBody = newC_body();
	 C_node *newNode;
	 */
	while (lexer(token) != 0 && !isNewLine(token)) {
		printf(" %s", token);
		/*
		 newNode = newC_node();
		 setNodeValue(newNode, (void*)token);
		 addBodyNode(commentBody, newNode);
		 */
	}
	printf("\n");
	/*	setCommentBody(comment, commentBody);	*/

}

void function_args(C_function_declaration *function) {

	bool arg = true;
	int args = 0;

	while (lexer(token) != 0 && !isCRB(token)) {

		if (isVariable(token) && arg) {
			printf("function_arg:\n");
			printf("%s\n", token);
			C_node *newNode = newC_node();
			char *newString = newC_string(token);
			setNodeValue(newNode, (void*) newString, E_string);
			addFunctionArgument(function, newNode);
			arg = false;
			args++;
		} else if (isComma(token) && !arg) {
			arg = true;
		} else if (isSpace(token)) {
			continue;
		} else {
			syntax_error();
		}

	}

	if (arg && args != 0) {
		syntax_error();
	}

}

void solveFunction(C_body *body, char* name) {
	printf("function:\n");
	C_function_declaration *function = newC_function_declaration();

	printf("function_name:\n");
	if (isVariable(name)) {
		printf("%s\n", name);
		setFunctionName(function, name);
	} else {
		syntax_error();
	}

	function_args(function);

	while (lexer(token) != 0) {
		if (isSemicolon(token)) {
			C_node *node = newC_node();
			setNodeValue(node, function, E_function_declaration);
			addBodyNode(body, node);
			return;
			break;
		} else if (isSpace(token)) {
			continue;
		} else {
			syntax_error();
		}
	}

}

void function(C_body *body) {
	char name[strlen(token) + 1];
	strcpy(name, token);

	if (isVariable(token) || isIncOp(token) || isDecOp(token)) {
		while (lexer(token) != 0) {
			if (isORB(token)) {
				solveFunction(body, name);
				break;
			} else if (isSpace(token)) {
				continue;
			} else if (isIncOp(name) || isDecOp(name)) {

				O_operation *oper = solveIncAndDec(name, token, true);

				C_node *newNode = newC_node();
				setNodeValue(newNode, (void*) oper, E_operation);
				addBodyNode(body, newNode);

				lexer(token);
				if (isSemicolon(token)) {
					break;
				} else {
					syntax_error();
				}

			} else if (isIncOp(token) || isDecOp(token)) {

				O_operation *oper = solveIncAndDec(token, name, false);

				C_node *newNode = newC_node();
				setNodeValue(newNode, (void*) oper, E_operation);
				addBodyNode(body, newNode);

				lexer(token);
				if (isSemicolon(token)) {
					break;
				} else {
					syntax_error();
				}

			} else {
				assignVariable(body, name);
				break;
			}
		}
	} else {
		syntax_error();
	}

}
